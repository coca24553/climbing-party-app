import 'package:flutter/material.dart';

class PageIndex extends StatelessWidget {
  const PageIndex({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          '산악회 모임',
          style: TextStyle(
            fontFamily: "font_02",
          ),
        ),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.menu),
        ),
        backgroundColor: Colors.grey,
        foregroundColor: Colors.white,
      ),

      body:
      SingleChildScrollView(
        child: Column(
          children: [
            Text(
              '소개',
              style: TextStyle(
                fontFamily: "font_01",
                fontSize: 32,
                fontWeight: FontWeight.bold,
                height: 3,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),

              child: Column(
                children: [
                  Image.asset(
                    'assets/images/main_01.jpeg',
                  ),
                  Text(
                    '등산 사랑회',
                    style: TextStyle(
                      fontFamily: "font_01",
                      fontSize: 20,
                      height: 3,
                    ),
                  ),
                  Text(
                    '회비 : 10,000',
                    style: TextStyle(
                      fontFamily: "font_03",
                      color: Colors.grey,
                    ),
                  ),
                  Text(
                    '매주 토요일 새벽 6시 출발',
                    style: TextStyle(
                      fontFamily: "font_03",
                      color: Colors.grey,
                    ),
                  ),

                  Text(
                    '주요 멤버',
                    style: TextStyle(
                      fontFamily: "font_01",
                      fontSize: 30,
                      height: 3,
                    ),
                  ),

                  Container(
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              Column(
                                children: [
                                  Image.asset(
                                    'assets/images/member_01.jpg',
                                    height: 400,
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(10),
                                    child: Column(
                                      children: [
                                        Text(
                                          '회장',
                                          style: TextStyle(
                                              fontFamily: "font_01",
                                              fontSize: 20
                                          ),
                                        ),
                                        Text(
                                          '오해원',
                                          style: TextStyle(
                                            fontFamily: "font_03",
                                            fontSize: 18,
                                            color: Colors.grey,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),

                        Container(
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    Column(
                                      children: [
                                        Image.asset(
                                          'assets/images/member_02.jpg',
                                          height: 400,
                                        ),
                                        Container(
                                          margin: EdgeInsets.all(10),
                                          child: Column(
                                            children: [
                                              Text(
                                                '부회장',
                                                style: TextStyle(
                                                  fontFamily: "font_01",
                                                  fontSize: 20,
                                                ),
                                              ),
                                              Text(
                                                '태연',
                                                style: TextStyle(
                                                  fontFamily: "font_03",
                                                  fontSize: 18,
                                                  color: Colors.grey,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
